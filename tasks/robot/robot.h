#pragma once

#include "topology.h"

bool PathExists(Topology& topology, const Point& start, const Point& finish);
