# Лекции

### 07.09.2021
- Введение
- Простейшая программа
- Простой ввод-вывод

[Запись](https://disk.yandex.ru/i/RkMpphs0PId10Q)

### 09.09.2021
- Примитивные типы
- `std::string`
- `std::vector`

[Запись](https://disk.yandex.ru/i/g_0baGiFEczhBw)

### 14.09.2021
- Ключевое слово `auto`
- Константность
- Функции и передача параметров
- Ссылки
- Указатели

[Запись](https://disk.yandex.ru/i/mxAXIlSYoL2DLQ)

### 16.09.2021
- Последовательные контейнеры

[Запись](https://disk.yandex.ru/i/JmVAMgDBIxAZig)

### 21.09.2021
- Ассоциативные контейнеры

[Запись](https://disk.yandex.ru/i/1dQB920fMEZy0A)

### 23.09.2021
- Виды памяти в C++
- Операторы `new` и `delete`

[Запись](https://disk.yandex.ru/d/UahT37yPYuGX2Q)

### 28.09.2021
- Разбор КР1
- Неймспейсы и `using`/`typedef`
- Классы: поля/методы, конструкторы/деструкторы, `const` и `static` методы, `explicit`

[Запись](https://disk.yandex.ru/i/1JoRzmTlb_tV6Q)

### 30.09.2021
- Проблемы с точностью `double`
- Раздельная компиляция, `.h` и `.cpp` файлы
- Перегрузка операторов

[Запись](https://disk.yandex.ru/i/A_Nma4oH5EzgKg)

### 05.10.2021
- Конструкторы копирования и операторы присваивания
- move семантика
- RAII
- Умные указатели
- Исключения

[Запись](https://disk.yandex.ru/i/cTUlLfwrSSxbMQ)
